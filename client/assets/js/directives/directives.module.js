(function() {
	'use strict';

	/**
	* Directives Module
	*
	* Define the directives module
	*/
	angular.module('application.directives', []);
	
})();