(function() {
	'use strict';

	/**
	* Github Users Service 
	*/
	angular.module('application.services')
	.factory('GithubUsers', GithubUsers);

	GithubUsers.$inject = ['$resource'];

	function GithubUsers($resource) {
		return $resource('https://api.github.com/users/:name', { name: '@_name' }, {});
	}

})();