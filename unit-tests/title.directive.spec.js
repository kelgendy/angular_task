'use strict';

describe('application.directives', function() {

	beforeEach(module('application.directives'));

	describe('title directive', function() {
		var element, scope;

		beforeEach(inject(function($rootScope, $compile) {
			element = angular.element('<title>{{title}}</title>');

			scope = $rootScope;
			scope.title =  'Github Users';

			$compile(element)(scope);
			scope.$digest();
		}));

		it('should set the page title to "Github Users"', function() {
			var title = element[0];

			expect(title.innerHTML).toEqual('Github Users');
		})
	});
});