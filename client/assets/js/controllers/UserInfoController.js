(function() {
	'use strict';

	angular.module('application.controllers')
	.controller('UserInfoController', UserInfoController);

	UserInfoController.$inject = ['$stateParams', 'GithubUsers'];

	function UserInfoController($stateParams, GithubUsers) {
		/* jshint validthis: true */
		var vm = this;

		vm.getUser = getUser;

		getUser();

		function getUser() {
			GithubUsers.get({name: $stateParams.name}, function(data) {
				vm.userInfo = data;
			});
		}
	}

})();