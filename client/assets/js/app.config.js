(function() {
	'use strict';

	angular.module('application')
	.config(config);

	config.$inject = ['$stateProvider', '$urlRouterProvider', '$locationProvider'];

	function config($stateProvider, $urlProvider, $locationProvider) {
		$stateProvider
		.state('home', {
			url: '/',
			templateUrl: 'templates/home.html',
			data: {pageTitle: 'Home - Github Users'}
		})
		.state('about', {
			url: '/about',
			templateUrl: 'templates/about.html',
			data: {pageTitle: 'About - Github Users'}
		})
		.state('users', {
			url: '/users',
			templateUrl: 'templates/users.html',
			controller: 'UserListController as userList',
			data: {pageTitle: 'Users - Github Users'}
		})
		.state('users.user', {
			url: '/:name',
			templateUrl: 'templates/userInfo.html',
			controller: 'UserInfoController as user',
			data: {pageTitle: 'User Info - Github Users'}
		});

		$urlProvider.otherwise('/');

		$urlProvider.rule(function($injector, $location) {

			var path = $location.path();
			var hasTrailingSlash = path[path.length-1] === '/';

			if(hasTrailingSlash) {
				//if last charcter is a slash, return the same url without the slash  
				var newPath = path.substr(0, path.length - 1); 
				return newPath; 
			} 

		});

		$locationProvider.html5Mode({
			enabled: false,
			requireBase: false
		});
	}
	
})();