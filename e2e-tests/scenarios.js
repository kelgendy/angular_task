'use strict';

describe('Github Users Application', function() {
	it("should redirect '/home' to '/' ", function() {
		browser.get('#/home');
		expect(browser.getLocationAbsUrl()).toBe('/');
	});

	it("should redirect any not found url to '/' ", function() {
		browser.get('#/someinvalidurl');
		expect(browser.getLocationAbsUrl()).toBe('/');
	});

	describe('Users List View', function() {
		beforeEach(function() {
			browser.get('#/users');
		});

		it('should get the first 30 users from github api and then load 30 more', function() {
			var usersList = element.all(by.repeater('user in userList.users'));
			expect(usersList.count()).toBe(30);

			var loadMore = element(by.css('button'));
			loadMore.click();

			expect(usersList.count()).toBe(60);
		});
	});

	describe('Users Info View', function() {
		beforeEach(function() {
			browser.get('#/users');
		});

		it('should redirect to user info url and display user info', function() {
			element.all(by.repeater('user in userList.users')).then(function(users) {
				var url = users[2].element(by.css('.user-thumb'));

				url.click();
				expect(browser.getLocationAbsUrl()).toBe('/users/pjhyett');
			});
		});

	});

});