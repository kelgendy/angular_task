'use strict';

describe('GithubUsers', function() {
	var $httpBackend;
	var GithubUsers;
	var usersData = [{name: 'user 1'}, {name: 'user 2'}, {name: 'user 3'}];

	beforeEach(function() {
		jasmine.addCustomEqualityTester(angular.equals);
	});

	beforeEach(module('application.services'));

	beforeEach(inject(function(_GithubUsers_, _$httpBackend_) {
		$httpBackend = _$httpBackend_;
		$httpBackend.whenGET(/\.html$/).respond('');
		$httpBackend.expectGET('https://api.github.com/users').respond(usersData);

		GithubUsers = _GithubUsers_;
	}));

	afterEach(function() {
		$httpBackend.verifyNoOutstandingExpectation();
		$httpBackend.verifyNoOutstandingRequest();
	});

	it('should fetch the users data from http://api.github.com/users', function() {

		var users = GithubUsers.query();

		expect(users).toEqual([]);

		$httpBackend.flush();
		expect(users).toEqual(usersData);
	});

});