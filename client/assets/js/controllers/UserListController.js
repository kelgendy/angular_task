(function() {
	'use strict';

	angular.module('application.controllers')
	.controller('UserListController', UserListController);

	UserListController.$inject = ['$timeout', '$state', 'GithubUsers'];

	function UserListController($timeout, $state, GithubUsers) {
		/* jshint validthis: true */
		var vm = this;

		vm.getUsers = getUsers;
		vm.loadMore = loadMore;
		vm.users = [];

		getUsers();

		function getUsers() {
			GithubUsers.query(function(data) {
				vm.users = data;
				vm.lastIndex = data[data.length - 1].id;

				$state.go("users.user", {name: data[0].login});
			});
		}

		function loadMore(index) {
			// disable loadMore button while processing request
			vm.loading = true;

			GithubUsers.query({since: index}, function(data) {
				angular.forEach(data, function(val, k) {
					vm.users.push(val);
				});
				vm.lastIndex = data[data.length - 1].id;

				$timeout(function() {
					var scroller = document.getElementById("users-list");
					scroller.scrollTop = scroller.scrollHeight;
				}, 0, false);

				// re-enable loadMore button
				vm.loading = false;
			});
		}
	}

})();