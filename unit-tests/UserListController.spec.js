'use strict';

describe('application.controllers', function() {

	beforeEach(module('application'));
	beforeEach(module('application.controllers'));

	describe('UserListController', function() {
		var $httpBackend, ctrl;
		var usersData = [{id: 1}, {id: 2}, {id: 3}];

		beforeEach(inject(function($controller, _$httpBackend_) {
			$httpBackend = _$httpBackend_;
			$httpBackend.whenGET(/\.html$/).respond('');
			$httpBackend.expectGET('https://api.github.com/users').respond(usersData);

			ctrl = $controller('UserListController');
		}));

		it('should create 3 users', function() {
			jasmine.addCustomEqualityTester(angular.equals);

			expect(ctrl.users).toEqual([]);

			$httpBackend.flush();
			expect(ctrl.users).toEqual(usersData);
		});

		it('should set lastIndex to the last id created "3"', function() {
			$httpBackend.flush();
			expect(ctrl.lastIndex).toEqual(3);
		});

	});

});