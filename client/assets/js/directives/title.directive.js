(function() {
	'use strict';

	/**
	* Title Directive 
	*/
	angular.module('application.directives')
	.directive('title', title);

	title.$inject = ['$rootScope', '$timeout'];

	function title($rootScope, $timeout) {
		return {
			link: function() {

				var listener = function(event, toState) {

					$timeout(function() {
						$rootScope.title = (toState.data && toState.data.pageTitle) ? toState.data.pageTitle : 'Github Users';
					});
				};

				$rootScope.$on('$stateChangeSuccess', listener);
			}
		};
	}

})();