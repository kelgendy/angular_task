(function() {
	'use strict';

	angular.module('application', [
		'ui.router',

		//foundation
		'foundation',
		// 'foundation.dynamicRouting',
		// 'foundation.dynamicRouting.animations',

		'application.controllers',
		'application.directives',
		'application.services',
	]);

})();
