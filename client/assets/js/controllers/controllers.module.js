(function() {
	'use strict';

	/**
	* Controllers Module
	*
	* Define the controllers module
	*/
	angular.module('application.controllers', []);
	
})();