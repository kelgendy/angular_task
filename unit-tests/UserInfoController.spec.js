'use strict';

describe('application.controllers', function() {

	beforeEach(module('application'));
	beforeEach(module('application.controllers'));

	describe('UserInfoController', function() {
		var $httpBackend, ctrl;
		var userInfo = {
			id: 1,
			name: 'user 1'
		};

		beforeEach(inject(function($controller, _$httpBackend_, $stateParams) {
			$httpBackend = _$httpBackend_;
			$httpBackend.whenGET(/\.html$/).respond('');
			$httpBackend.expectGET('https://api.github.com/users/user1').respond(userInfo);

			$stateParams.name = 'user1';

			ctrl = $controller('UserInfoController');
		}));

		it('should fetch the user info', function() {
			jasmine.addCustomEqualityTester(angular.equals);

			$httpBackend.flush();
			expect(ctrl.userInfo).toEqual(userInfo);
		});

	});
});