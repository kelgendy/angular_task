(function() {
	'use strict';

	/**
	* Services Module
	*
	* Define the services module
	*/
	angular.module('application.services', ['ngResource']);

})();